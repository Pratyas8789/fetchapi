let user = ("https://jsonplaceholder.typicode.com/users")

fetch(user)
    .then((response) => {
        return response.json()
    })
    .then((response) => {
        return Promise.all([userAndItsDetails(response)])
    })
    .then((arrayOfUsersAndItsDetails)=>{
        console.log(arrayOfUsersAndItsDetails[0]);
    })
    .catch((error) => {
        console.error(error.message);
    })

const userAndItsDetails = (response) => {
    const userAndItsDetails = {};
    for (let data of response) {
        let name = data.name
        userAndItsDetails[name] = data
        delete userAndItsDetails.name
    }
    return userAndItsDetails;
}