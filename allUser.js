let user=("https://jsonplaceholder.typicode.com/users")

fetch(user)
.then((response)=>{
    return response.json()
})
.then((data)=>{
    printAllUser(data)
})
.catch((error)=>{
    console.error(error.message);
})


const printAllUser=(data)=>{
    const allUserName=data.map(user=> user)
    console.log(allUserName);
}