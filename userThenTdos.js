const user = "https://jsonplaceholder.typicode.com/users";
const toDo = "https://jsonplaceholder.typicode.com/todos";

fetch(user)
    .then((response) => {
        return response.json();
    })
    .then((response1) => {
        return Promise.all([printAllTodos(),response1])
    })
    .then((arrayOfUserAndTodos)=>{
        console.log(arrayOfUserAndTodos[1],arrayOfUserAndTodos[0]);
    })
    .catch((error) => {
        console.error(error);
    })

function printAllTodos() {
    return fetch(toDo)
        .then((response2) => {
            return response2.json();
        })
        .catch((error) => {
            console.error(error);
        })
}