let user=("https://jsonplaceholder.typicode.com/todos")

fetch(user)
.then((response)=>{
    return response.json()
})
.then((data)=>{
    printAllTodos(data)
})
.catch((error)=>{
    console.error(error.message);
})


const printAllTodos=(data)=>{
    const allTodos=data.map(todo=> todo)
    console.log(allTodos);
}