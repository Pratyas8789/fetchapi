const toDo = "https://jsonplaceholder.typicode.com/todos";
let user = ("https://jsonplaceholder.typicode.com/users")

fetch(toDo)
    .then((responseFromTodo) => {
        return responseFromTodo.json()
    })
    .then((dataFromTodo) => {
        fetchUser(dataFromTodo);
    })
    .catch((error) => {
        console.error(error.message);
    })


function fetchUser(dataFromTodo) {
    fetch(user)
        .then((responseFromUser) => {
            return responseFromUser.json()
        })
        .then((dataFromUser) => {
            return Promise.all([firstTodosAndItsDetails(dataFromTodo, dataFromUser)])
        })
        .then((arrayOfFirstTodosAndItsDetails)=>{
            console.log(arrayOfFirstTodosAndItsDetails[0]);
        })
        .catch((error) => {
            console.error(error.message);
        })
}

firstTodosAndItsDetails = (dataFromTodo, dataFromUser) => {
    const firstTodo = dataFromTodo[0].title;
    const firstUserId = dataFromTodo[0].userId;

    const userDeatils = dataFromUser.filter(data => data.id === firstUserId)

    const firstTodosAndItsDetails = {}

    firstTodosAndItsDetails[firstTodo] = userDeatils
    return firstTodosAndItsDetails;
}